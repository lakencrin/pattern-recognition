from sklearn import datasets, tree
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score
from random import randint
from graphviz import Source

def rand():
    return randint(0, 149)

# загрузка датасета
breast_cancer = datasets.load_breast_cancer()

# подготовка массивов 
entry = []
target = []
testentry = []
testtarget = []
ordinals = []

# распределение образцов между обучающей и тестовой выборкой
for i in range(105):
    x = rand()
    while ordinals.__contains__(x):
        x = rand()
    ordinals.append(x)
    entry.append(breast_cancer.data[x])
    target.append(breast_cancer.target[x])

for i in range(150):
    if not ordinals.__contains__(i):
        testentry.append(breast_cancer.data[i])
        testtarget.append(breast_cancer.target[i])

# инициализация дерева решений
clf = tree.DecisionTreeClassifier()
# построение дерева решений
clf.fit(entry, target)
# получение результатов предсказания классификатором
array = clf.predict(testentry)

# получение дерева решений в формате DOT
dot = tree.export_graphviz(clf)
# визуализация графа средствами библиотеки graphviz
source = Source(dot, filename="maintree")
source.render(format='png', view=False)

# оценка классификации и вывод результата в консоль
ascore = accuracy_score(array, testtarget)
pscore = precision_score(array, testtarget, average='micro')
rscore = recall_score(array, testtarget, average='micro')
fscore = f1_score(array, testtarget, average='micro')
print(ascore, pscore, rscore, fscore)

