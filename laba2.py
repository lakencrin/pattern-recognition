from sklearn import datasets
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
from matplotlib import pyplot

# загрузка датасета
wine = datasets.load_wine()

first = 0
second = 6

# инициализация вспомогательных массивов
X = wine.data[:,[first, second]]
Y = wine.target

# визуализация исходных данных
fig, axes = pyplot.subplots(1, 1, figsize=(8,4))
axes.scatter(X[:,0], X[:,1], c=Y)
axes.set_xlabel(wine.feature_names[first], fontsize=14)
axes.set_ylabel(wine.feature_names[second], fontsize=14)
pyplot.show()

# инициализация кластеризатора
kmeans = KMeans(n_clusters = 3)
# кластеризация
kmeans.fit(X)
# получение результатов
array = kmeans.labels_

# визуализация полученных данных
fig, axes = pyplot.subplots(1, 1, figsize=(8,4))
axes.scatter(X[:,0], X[:,1], c=array)
axes.set_xlabel(wine.feature_names[first], fontsize=14)
axes.set_ylabel(wine.feature_names[second], fontsize=14)
pyplot.show()

score = adjusted_rand_score(Y, array)
print(score)
